from django.http import HttpResponse, Http404
from django.shortcuts import render

from tweets.models import Tweet

# Create your views here.
def hello(request):
    return HttpResponse(content="Hello World", status=200)


def index(request):
    tweets = Tweet.objects.all()
    username = request.GET.get('username')

    if username:
        tweets = tweets.filter(user__username=username)

    ctx = {
        'tweets': tweets
    }

    return render(request, 'tweets/index.html', ctx)


def detail(request, pk):
    try:
        tweet = Tweet.objects.get(pk=pk)
    except Tweet.DoesNotExist:
        raise Http404

    ctx = {
        'tweet': tweet
    }

    return render(request, 'tweets/detail.html', ctx)