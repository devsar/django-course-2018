from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Tweet(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    text = models.CharField(max_length=140)
    created = models.DateTimeField(auto_now_add=True)
    likes = models.IntegerField(default=0)
    hashtags = models.ManyToManyField("tweets.Hashtag", related_name="tweets")

    def __str__(self):
        return "[{user}] {text}".format(user=self.user, text=self.text)

    def get_hashtags(self):
        hashtags = []

        for word in self.text.split(' '):
            if word.startswith('#'):
                hashtags.append(word)

        return hashtags

    def like(self):
        self.likes += 1
        self.save()

    def unlike(self):
        self.likes = max(0, self.likes - 1)
        self.save()


class Comment(models.Model):
    tweet = models.ForeignKey(Tweet, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    text = models.CharField(max_length=255)
    created = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.text


class Hashtag(models.Model):
    value = models.CharField(max_length=255)

    def __str__(self):
        return self.value

    @property
    def without_hash(self):
        return self.value[1:]